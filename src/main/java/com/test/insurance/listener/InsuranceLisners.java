package com.test.insurance.listener;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import com.test.insurance.entity.Insurance;
import com.test.insurance.repository.InsuranceRepository;
import com.test.jms.InsuranceRquestDTO;
import com.test.jms.PaymentRequestDTO;
import com.test.jms.PaymentResponseDTO;



@Component
public class InsuranceLisners {
	
		@Autowired
		InsuranceRepository insuranceRepository;
		
		@Autowired
		private JmsMessagingTemplate jmsMessagingTemplate;

		@JmsListener(destination = "insurance", containerFactory = "queueConnectionFactory")
		public void receiveQueue(InsuranceRquestDTO insuranceRquestDTO) {
					Insurance insurance = new Insurance(); 
				  insurance.setStatus("PENDING");
				  insurance.setTicketId(insuranceRquestDTO.getTicketId());
				  insurance.setInsurancePrice(100D);
				  insurance.setPrice(insuranceRquestDTO.getPrice());
				  insurance.setTotalPrice(insuranceRquestDTO.getPrice() - 100D);
				  insurance.setUserId(insuranceRquestDTO.getUserId());
				  
				  Insurance persistedInsurance = insuranceRepository.save(insurance);
				 
				  
				  PaymentRequestDTO paymentRequestDTO = new PaymentRequestDTO();
				  BeanUtils.copyProperties(persistedInsurance, paymentRequestDTO);
				  paymentRequestDTO.setInsuranceId(persistedInsurance.getId());
				  this.jmsMessagingTemplate.convertAndSend(new ActiveMQQueue("payment"),paymentRequestDTO);
				System.out.println(persistedInsurance);
		}
		
		@JmsListener(destination = "paymentResponse", containerFactory = "topicConnectionFactory") 
		public void receiveQueue(PaymentResponseDTO paymentResponseDTO) {
			
			System.out.println("update :"+paymentResponseDTO);

			if (paymentResponseDTO.getStatus().equals("DONE")) {			
		
				Insurance insurance = insuranceRepository.findByTicketId(paymentResponseDTO.getTicketId());	
				insurance.setStatus("COMPLETED");
				insuranceRepository.save(insurance);
			
			} 
			
		}
}
